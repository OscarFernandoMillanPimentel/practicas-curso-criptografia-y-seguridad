# Autor: Millán Pimentel Oscar Fernando

import sys

print('Antes de empezar asegurese que el Mensaje en Cifrado este a la altura de \'Main.py\'')

entrada = ''
malware = ''

if len(sys.argv) == 3:
    entrada = sys.argv[1]
    malware = sys.argv[2]

    if malware[len(malware)-4] == '.':
        print('\nArchivo a Desencriptar: '+entrada+'\nArchivo de Salida: '+malware+'\n')
    elif not '.bin' in malware:
        print('\nArchivo a Desencriptar: '+entrada+'\nArchivo de Salida: '+malware+'.bin\n')
    elif '.bin' in malware:
        print('\nArchivo a Desencriptar: '+entrada+'\nArchivo de Salida: '+malware+'\n')

else:
    print('\nERROR: Introdujo uno (1) o más de dos (2) argumentos')
    print('SOLUCIÓN: Introduce los argumentos correctamente')
    print('Ejemplo: entrada_dos_argumentos.py \'Nombre.VBN\' \'Nombre de Salida.bin\'\n')

print('Encontrando la llave (1 byte) por fuerza bruta...')

cifrado = ''

with open(entrada,'rb') as archivo:
    cifrado = archivo.read().hex()

if malware[len(malware)-4] == '.':
    f = open(malware,'w')
elif not '.bin' in malware:
    f = open(malware+'bin','w')
elif '.bin' in malware:
    f = open(malware,'w')

for key in range(256):
    count = 0
    hexor = ''
    for i in range(int(len(cifrado)/2)):
        hexor = hexor + chr(int(hex(int(cifrado[count] + cifrado[count+1],16)^key)[2:],16))
        count+=2
    
    if 'program cannot' in hexor:
        print(f'La llave es: {hex(key)[2:]}')
        print(f'Descifrando el archivo con la llave \'{hex(key)[2:]}\' encontrada y la operacion XOR ...')
        print(f'Los datos descifrados se guardaron en el archivo {malware}')
        break

f.write(hexor)
f.close()