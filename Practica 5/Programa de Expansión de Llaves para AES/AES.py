# funcion que rota los bytes de la palabra recibida
def rotWord(key):

    aux_key = ''

    if '0x' in str(key):
        aux_key = str(key)[2:]
    else:
        aux_key = str(key)
        
    rw = aux_key[2:]+aux_key[:2]

    return rw

# funcion que substituye los bytes con forme a la S-box
def subWord(key):
    # variables que dividen la parte de la key recibida en bytes
    p1 = str(key)[0:2]
    p2 = str(key)[2:4]
    p3 = str(key)[4:6]
    p4 = str(key)[6:]

    # variable que almacenará el resultado de aplicar la subsitución de bytes
    sw = ''

    # Lista S-Box para la sustitución de bytes para la función SubWord
    s_box = ['63','7c','77','7b','f2','6b','6f','c5','30','01','67','2b','fe','d7','ab','76',
             'ca','82','c9','7d','fa','59','47','f0','ad','d4','a2','af','9c','a4','72','c0',
             'b7','fd','93','26','36','3f','f7','cc','34','a5','e5','f1','71','d8','31','15',
             '04','c7','23','c3','18','96','05','9a','07','12','80','e2','eb','27','b2','75',
             '09','83','2c','1a','1b','6e','5a','a0','52','3b','d6','b3','29','e3','2f','84',
             '53','d1','00','ed','20','fc','b1','5b','6a','cb','be','39','4a','4c','58','cf',
             'd0','ef','aa','fb','43','4d','33','85','45','f9','02','7f','50','3c','9f','a8',
             '51','a3','40','8f','92','9d','38','f5','bc','b6','da','21','10','ff','f3','d2',
             'cd','0c','13','ec','5f','97','44','17','c4','a7','7e','3d','64','5d','19','73',
             '60','81','4f','dc','22','2a','90','88','46','ee','b8','14','de','5e','0b','db',
             'e0','32','3a','0a','49','06','24','5c','c2','d3','ac','62','91','95','e4','79',
             'e7','c8','37','6d','8d','d5','4e','a9','6c','56','f4','ea','65','7a','ae','08',
             'ba','78','25','2e','1c','a6','b4','c6','e8','dd','74','1f','4b','bd','8b','8a',
             '70','3e','b5','66','48','03','f6','0e','61','35','57','b9','86','c1','1d','9e',
             'e1','f8','98','11','69','d9','8e','94','9b','1e','87','e9','ce','55','28','df',
             '8c','a1','89','0d','bf','e6','42','68','41','99','2d','0f','b0','54','bb','16']

    # print(f'Rotword = '+p1+' '+p2+' '+p3+' '+p4)
    
    # adicion de la sustitución de bytes de la key recibida a la encontrada en la S-box 
    sw = sw + s_box[((15+1)*(int(p1[0],16)))+(int(p1[1],16))]
    sw = sw + s_box[((15+1)*(int(p2[0],16)))+(int(p2[1],16))]
    sw = sw + s_box[((15+1)*(int(p3[0],16)))+(int(p3[1],16))]
    sw = sw + s_box[((15+1)*(int(p4[0],16)))+(int(p4[1],16))]
    
    return sw

# key is byte type
# w is word type
def keyExpansion(w_key, w, nk):

    # block size
    nb = 4

    # number of rounds
    nr = 10

    # w = []

    # prints comentados (descomentar para asegurarse que sea la llave correcta la que se ingresa)
    # print('\nw_key: '+w_key+'\n')
    # print(f'key: {key}\n') 

    # temp is word type
    temp = 0

    i = 0

    while(i < nk):
        w[i] = w_key[8*i:8*(i+1)]
        i = i+1
    
    # print(f'W[] = {w}')

    i = nk

    print('  i     temp       After        After        Rcon[i/nk]    After XOR    w[i-Nk]    w[i]=temp XOR')
    print('(dec)              RotWord()    SubWord()                  with Rcon               w[i-Nk]      ')
    print('------------------------------------------------------------------------------------------------')
    while(i < nb * (nr+1)):
        temp = w[i-1]
        if i < 10:
            lineprint = '  0'+str(i)+'    '+str(temp)
        else:
            lineprint = '  '+str(i)+'    '+str(temp)

        if (i % nk) == 0:
            aux_rcon = int(2**((i/4)-1))
            if aux_rcon < 256:
                aux_rcon = int(2**((i/4)-1))
            elif aux_rcon == 256:
                aux_rcon = 432
            else:
                aux_rcon = 864
            
            if aux_rcon < 16:
                rcon = int(str(hex(aux_rcon))+'000000',16)
            elif aux_rcon <= 128:
                rcon = int(str(hex(aux_rcon))+'000000',16)
            else:
                rcon = int(str(hex(aux_rcon))+'00000',16)

            rw = rotWord(temp)
            lineprint = lineprint+'   '+rw

            sw = subWord(rw)
            lineprint = lineprint+'     '+sw

            temp = str(hex( int(sw,16) ^ rcon) )[2:]

            if aux_rcon < 16:
                lineprint = lineprint+'      0'+str(hex(rcon))[2:]
            else:
                lineprint = lineprint+'      '+str(hex(rcon))[2:]

            
            lineprint = lineprint+'     '+temp
        # comentado porque no se usa en este caso, solo para llaves de mas de 128 bits
        # elif nk > 6 and (i % nk) == 4:
        #     temp = subWord(temp)
        if i % 4 == 0:
            lineprint = lineprint+'     '+w[i-nk]
        else:
            lineprint = lineprint+'                                                        '+w[i-nk]

        w[i] = str(hex(int(w[i-nk],16) ^ int(temp,16)))[2:]

        lineprint = lineprint+'    '+w[i]
        
        print(lineprint)
        i = i+1
    
    

key = "2b7e151628aed2a6abf7158809cf4f3c"

w = [0]*(4*(10+1))

keyExpansion(key,w,4)

# sw = subWord(rotWord('2a6c7605'))

# print(f'subword(rotword(temp)) = {sw}')

# xors = hex(int('0x12',16) ^ int('0x13',16))

# print(f'xor = {xors}')