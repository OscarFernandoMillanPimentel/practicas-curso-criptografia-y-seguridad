#################### ALGORITMO DE EXPANSION DE LLAVES - AES ################

#### Variables Globales ###

# block size
nb = 4

# number of rounds
nr = 10

# Lista S-Box para la sustitución de bytes para la función SubWord
s_box = ['63','7c','77','7b','f2','6b','6f','c5','30','01','67','2b','fe','d7','ab','76',
        'ca','82','c9','7d','fa','59','47','f0','ad','d4','a2','af','9c','a4','72','c0',
        'b7','fd','93','26','36','3f','f7','cc','34','a5','e5','f1','71','d8','31','15',
        '04','c7','23','c3','18','96','05','9a','07','12','80','e2','eb','27','b2','75',
        '09','83','2c','1a','1b','6e','5a','a0','52','3b','d6','b3','29','e3','2f','84',
        '53','d1','00','ed','20','fc','b1','5b','6a','cb','be','39','4a','4c','58','cf',
        'd0','ef','aa','fb','43','4d','33','85','45','f9','02','7f','50','3c','9f','a8',
        '51','a3','40','8f','92','9d','38','f5','bc','b6','da','21','10','ff','f3','d2',
        'cd','0c','13','ec','5f','97','44','17','c4','a7','7e','3d','64','5d','19','73',
        '60','81','4f','dc','22','2a','90','88','46','ee','b8','14','de','5e','0b','db',
        'e0','32','3a','0a','49','06','24','5c','c2','d3','ac','62','91','95','e4','79',
        'e7','c8','37','6d','8d','d5','4e','a9','6c','56','f4','ea','65','7a','ae','08',
        'ba','78','25','2e','1c','a6','b4','c6','e8','dd','74','1f','4b','bd','8b','8a',
        '70','3e','b5','66','48','03','f6','0e','61','35','57','b9','86','c1','1d','9e',
        'e1','f8','98','11','69','d9','8e','94','9b','1e','87','e9','ce','55','28','df',
        '8c','a1','89','0d','bf','e6','42','68','41','99','2d','0f','b0','54','bb','16']

tabla_L =  ['__','00','19','01','32','02','1A','C6','4B','C7','1B','68','33','EE','DF','03',
            '64','04','E0','0E','34','8D','81','EF','4C','71','08','C8','F8','69','1C','C1',
            '7D','C2','1D','B5','F9','B9','27','6A','4D','E4','A6','72','9A','C9','09','78',
            '65','2F','8A','05','21','0F','E1','24','12','F0','82','45','35','93','DA','8E',
            '96','8F','DB','BD','36','D0','CE','94','13','5C','D2','F1','40','46','83','38',
            '66','DD','FD','30','BF','06','8B','62','B3','25','E2','98','22','88','91','10',
            '7E','6E','48','C3','A3','B6','1E','42','3A','6B','28','54','FA','85','3D','BA',
            '2B','79','0A','15','9B','9F','5E','CA','4E','D4','AC','E5','F3','73','A7','57',
            'AF','58','A8','50','F4','EA','D6','74','4F','AE','E9','D5','E7','E6','AD','E8',
            '2C','D7','75','7A','EB','16','0B','F5','59','CB','5F','B0','9C','A9','51','A0',
            '7F','0C','F6','6F','17','C4','49','EC','D8','43','1F','2D','A4','76','7B','B7',
            'CC','BB','3E','5A','FB','60','B1','86','3B','52','A1','6C','AA','55','29','9D',
            '97','B2','87','90','61','BE','DC','FC','BC','95','CF','CD','37','3F','5B','D1',
            '53','39','84','3C','41','A2','6D','47','14','2A','9E','5D','56','F2','D3','AB',
            '44','11','92','D9','23','20','2E','89','B4','7C','B8','26','77','99','E3','A5',
            '67','4A','ED','DE','C5','31','FE','18','0D','63','8C','80','C0','F7','70','07']

tabla_E =  ['01','03','05','0F','11','33','55','FF','1A','2E','72','96','A1','F8','13','35',
            '5F','E1','38','48','D8','73','95','A4','F7','02','06','0A','1E','22','66','AA',
            'E5','34','5C','E4','37','59','EB','26','6A','BE','D9','70','90','AB','E6','31',
            '53','F5','04','0C','14','3C','44','CC','4F','D1','68','B8','D3','6E','B2','CD',
            '4C','D4','67','A9','E0','3B','4D','D7','62','A6','F1','08','18','28','78','88',
            '83','9E','B9','D0','6B','BD','DC','7F','81','98','B3','CE','49','DB','76','9A',
            'B5','C4','57','F9','10','30','50','F0','0B','1D','27','69','BB','D6','61','A3',
            'FE','19','2B','7D','87','92','AD','EC','2F','71','93','AE','E9','20','60','A0',
            'FB','16','3A','4E','D2','6D','B7','C2','5D','E7','32','56','FA','15','3F','41',
            'C3','5E','E2','3D','47','C9','40','C0','5B','ED','2C','74','9C','BF','DA','75',
            '9F','BA','D5','64','AC','EF','2A','7E','82','9D','BC','DF','7A','8E','89','80',
            '9B','B6','C1','58','E8','23','65','AF','EA','25','6F','B1','C8','43','C5','54',
            'FC','1F','21','63','A5','F4','07','09','1B','2D','77','99','B0','CB','46','CA',
            '45','CF','4A','DE','79','8B','86','91','A8','E3','3E','42','C6','51','F3','0E',
            '12','36','5A','EE','29','7B','8D','8C','8F','8A','85','94','A7','F2','0D','17',
            '39','4B','DD','7C','84','97','A2','FD','1C','24','6C','B4','C7','52','F6','01']

################# FUNCIONES ###################
# funcion que rota los bytes de la palabra recibida
def rotWord(key):

    aux_key = ''

    if '0x' in str(key):
        aux_key = str(key)[2:]
    else:
        aux_key = str(key)
        
    rw = aux_key[2:]+aux_key[:2]

    return rw

# funcion que substituye los bytes con forme a la S-box
def subWord(key):
    # variables que dividen la parte de la key recibida en bytes
    p1 = str(key)[0:2]
    p2 = str(key)[2:4]
    p3 = str(key)[4:6]
    p4 = str(key)[6:]

    # variable que almacenará el resultado de aplicar la subsitución de bytes
    sw = ''

    # adicion de la sustitución de bytes de la key recibida a la encontrada en la S-box 
    sw = sw + s_box[((15+1)*(int(p1[0],16)))+(int(p1[1],16))]
    sw = sw + s_box[((15+1)*(int(p2[0],16)))+(int(p2[1],16))]
    sw = sw + s_box[((15+1)*(int(p3[0],16)))+(int(p3[1],16))]
    sw = sw + s_box[((15+1)*(int(p4[0],16)))+(int(p4[1],16))]
    
    return sw

# key is byte type
# w is word type
def keyExpansion(w_key, w, nk):

    temp = 0

    i = 0
    while(i < nk):
        w[i] = w_key[8*i:8*(i+1)]
        i = i+1

    i = nk

    # print('  i     temp       After        After        Rcon[i/nk]    After XOR    w[i-Nk]    w[i]=temp XOR')
    # print('(dec)              RotWord()    SubWord()                  with Rcon               w[i-Nk]      ')
    # print('------------------------------------------------------------------------------------------------')


    while(i < nb * (nr+1)):
        temp = w[i-1]
        
        if i < 10:
            lineprint = '  0'+str(i)+'    '+str(temp)
        else:
            lineprint = '  '+str(i)+'    '+str(temp)

        if (i % nk) == 0:
            aux_rcon = int(2**((i/4)-1))
            if aux_rcon < 256:
                aux_rcon = int(2**((i/4)-1))
            elif aux_rcon == 256:
                aux_rcon = 432
            else:
                aux_rcon = 864
            
            if aux_rcon < 16:
                rcon = int(str(hex(aux_rcon))+'000000',16)
            elif aux_rcon <= 128:
                rcon = int(str(hex(aux_rcon))+'000000',16)
            else:
                rcon = int(str(hex(aux_rcon))+'00000',16)

            rw = rotWord(temp)
            if len(rw) < 8:
                rw = '0'+rw

            lineprint = lineprint+'   '+rw

            sw = subWord(rw)
            lineprint = lineprint+'     '+sw

            temp = str(hex( int(sw,16) ^ rcon) )[2:]  

            while(len(temp) < 8):
                temp = '0'+temp

            if aux_rcon < 16:
                lineprint = lineprint+'      0'+str(hex(rcon))[2:]
            else:
                lineprint = lineprint+'      '+str(hex(rcon))[2:]

            lineprint = lineprint+'     '+temp

        if i % 4 == 0:
            lineprint = lineprint+'     '+w[i-nk]
        else:
            lineprint = lineprint+'                                                        '+w[i-nk]  

        w[i] = str(hex(int(w[i-nk],16) ^ int(temp,16)))[2:]
        
        while(len(w[i]) < 8):
            w[i] = '0'+w[i]
        
        lineprint = lineprint+'    '+w[i]
        # print(lineprint)
        i = i+1
    
    return w


########################## ALGORITMO DE CIFRADO - AES ##########################

########################## Funciones Auxiliares ##########################

# Funcion que convierte al entrada de cadena a arreglo de "bytes"
def strToBytesList(cadena):
    key = []

    for i in range(int(len(cadena)/2)):
        key.append(cadena[i*2]+cadena[i*2+1])
    
    return key

# Funcion que recibe una lista y devuelve una matriz 4x4   
def listBytesToMatrixBytes(list):
    matrix = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]

    for r in range(4):
        for c in range(nb):
            matrix[r][c] = list[r+4*c]

    return matrix

# Funcion que realiza XOR de 2 bytes en forma de string y devuelve el resultado en string
def xorToString(v1, v2):
    return str(hex(int(v1,16) ^ int(v2,16)))[2:]

# Funcion que aplica XOR a una lista de bytes en string y devuelve el resultado en string
def xorListToString(listHex):
    xor = 0

    for i in listHex:
        xor = xor ^ int(i,16)
    
    if len(str(hex(xor))[2:]) < 2:
        xor = '0'+str(hex(xor))[2:]
    else:
        xor = str(hex(xor))[2:]

    return xor

# Funcion que recibe un byte y devuelve su byte en la coordenada corespondiente
# de la tabla L en strin
def getByteFrom_L(byte):
    return  tabla_L[((15+1)*(int(byte[0],16)))+(int(byte[1],16))]

# Funcion que recibe un byte y devuelve su byte en la coordenada corespondiente
# de la tabla E en string
def getByteFrom_E(byte):
    return  tabla_E[((15+1)*(int(byte[0],16)))+(int(byte[1],16))]

# Funcio que recibe una matriz bidimensional y devuelve un string uniendo sus valores
# por coordenada
def matrixToString(state):
    string = ''

    for i in range(4):
        for j in range(4):
            string = string+state[j][i]

    return string

########################## Funciones Principales ##########################

# Funcion Add Round Key
def addRoundKey(matrix1, matrix2):
    matrix = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]

    for i in range(4):
        for j in range(4):
            xor = xorToString(matrix1[i][j], matrix2[i][j])
            if len(xor) < 2:
                matrix[i][j] = '0'+xor
            else:
                matrix[i][j] = xor

    return matrix

# Funcion Sub Bytes
def subBytes(state):
    # Variable que almacenará el nuevo 'state'
    new_state = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
    
    for i in range(4):
        for j in range(nb):
            p = state[i][j]
            new_state[i][j] = s_box[((15+1)*(int(p[0],16)))+(int(p[1],16))]

    return new_state

# Funcion Shift Rows
def shiftRows(state):
    # Variable que almacenará el nuevo 'state'
    new_state = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]

    new_state[0] = state[0]

    state[1].append(state[1][0])
    new_state[1] = state[1][1:]

    state[2].append(state[2][0])
    state[2].append(state[2][1])
    new_state[2] = state[2][2:]

    state[3].append(state[3][0])
    state[3].append(state[3][1])
    state[3].append(state[3][2])
    new_state[3] = state[3][3:]

    return new_state

# Funcion Mix Columns
def mixColumns(state):
    # Variable que almacenará el nuevo 'state'
    new_state = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]

    matriz_fija =  [['02','03','01','01'],
                    ['01','02','03','01'],
                    ['01','01','02','03'],
                    ['03','01','01','02']]

    valor = ''
    v_final = '00'

    for k in range(4):
        for j in range(4):
            for i in range(4):
                v1 = int(getByteFrom_L(matriz_fija[k][i]),16)
                v2 = int(getByteFrom_L(state[i][j]),16)
                valor = v1 + v2

                if valor > 255:
                    valor = valor % 255

                v = str(hex(valor))[2:]
                if len(v) < 2:
                    v = '0'+v

                valor = getByteFrom_E(v)
                v_final = xorToString(v_final, valor)

                if len(v_final) < 2:
                    v_final = '0'+v_final

            new_state[k][j] = v_final
            v_final = '00'

    return new_state

# Funcion que realiza el cifrado de AES
def cipher(b_in, b_out, w):

    print(f'round[ 0].input     {b_in}')

    index_subkey = 0
    subkeyStr = ''
    list_subkeys_w = []

    b_in_aux = strToBytesList(b_in)

    for j in range(int(len(w)/4)):
        for i in range(4):
            subkeyStr = subkeyStr + w[index_subkey]
            index_subkey += 1
        list_subkeys_w.append(subkeyStr)
        subkeyStr = ''
    
    print(f'round[ 0].k_sch     {list_subkeys_w[0]}')

    w_aux = strToBytesList(list_subkeys_w[0])
    
    state = listBytesToMatrixBytes(b_in_aux)

    subkey = listBytesToMatrixBytes(w_aux)

    state = addRoundKey(state, subkey)
    print(f'round[ 0].start     {matrixToString(state)}')

    p_round = 'round['
    for i in range(1,nr):

        state = subBytes(state)

        p_round = p_round+' '+str(i)+'].s_box     '+str(matrixToString(state))
        print(p_round)
        p_round = 'round['

        state = shiftRows(state)

        p_round = p_round+' '+str(i)+'].s_row     '+str(matrixToString(state))
        print(p_round)
        p_round = 'round['

        state = mixColumns(state)

        p_round = p_round+' '+str(i)+'].m_col     '+str(matrixToString(state))
        print(p_round)
        p_round = 'round['

        subkey = listBytesToMatrixBytes(strToBytesList(list_subkeys_w[i]))

        p_round = p_round+' '+str(i)+'].k_sch     '+str(matrixToString(subkey))
        print(p_round)
        p_round = 'round['

        state = addRoundKey(state, subkey)

        if i < 9:
            p_round = p_round+' '+str(i+1)+']'
        else:
            p_round = p_round+str(i+1)+']'

        p_round = p_round+'.start     '+str(matrixToString(state))
        print(p_round)
        p_round = 'round['

    state = subBytes(state)
    print(f'round[10].s_box     {matrixToString(state)}')

    state = shiftRows(state)
    print(f'round[10].s_row     {matrixToString(state)}')

    subkey = listBytesToMatrixBytes(strToBytesList(list_subkeys_w[nr]))
    print(f'round[10].k_sch     {matrixToString(subkey)}')

    state = addRoundKey(state, subkey)
    print(f'round[10].output    {matrixToString(state)}\n')

    b_out = state

############## Función Main ###############

# Funcion Principal que tiene los llamados a función de los algoritmos
def main():

    # Llave de prueba para apendice donde se encuentran las tablas
    # key = "2b7e151628aed2a6abf7158809cf4f3c"

    # Llave de Uso para salida deseada
    key = '000102030405060708090a0b0c0d0e0f'

    # Lista de tamaño 44 para usar en el algoritmo de expansión de llaves
    w = [0]*(4*(10+1))

    # Variable que guarda las llaves expandidas para el algoritmo de cifrado de AES
    lista_llaves = keyExpansion(key,w,4)

    # Plain Text de prueba para apendice donde se encuentran las tablas
    # plain_text = '3243f6a8885a308d313198a2e0370734'

    # Plain Text de prueba para apendice donde se encuentran las tablas
    plain_text = '00112233445566778899aabbccddeeff'

    # Prints que muestran el PLAINTEXT y la KEY
    print(f'\nPLAINTEXT: {plain_text}')
    print(f'KEY:       {key}\n\nCIPHER (ENCRYPT) :\n')

    # Llamado a función del algoritmo de cifrado de AES
    cipher(plain_text,[[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]] ,lista_llaves)

# Ejecución del código
main()