#!/usr/bin/env python3

# Paulo Contreras Flores
# paulo.contreras.flores@gmail.com

import pyaes, pbkdf2, binascii, os, secrets, base64, mysql.connector, SecureString


# Guardar los datos en una base de datos relacional
try:
    # Estos datos necesitan estar en un archivo separado 
    # del programa y tampoco deben subirse a git
    # El cifrado de la conexion se realizara en otra practica
    mydb = mysql.connector.connect(
                                 user="cripto",
                                 password="mypassword",
                                 host="127.0.0.1",
                                 port="3306",
                                 database="hospital")
    cursor = mydb.cursor()
    cursor.execute("SELECT * from expediente")
    data = cursor.fetchone()

except mysql.connector.Error as err:
  print("Something went wrong: {}".format(err))

finally:
    if mydb:
        #cursor.close()
        mydb.close()
        print("DBMS connection is closed")


# Solo descifra los datos del primer registro
# de la tabla expediente, se requiere agregar la
# funcionalidad para descifrar todos los registros
print("data",{
     'Id:': data[0],
     'Name:': data[1],
     'Diagnosis (base64):': data[2],
     'Medical treatment (base64):': data[3],
     'PasswordSalt': data[4],
     'IV': data[5],
     })



# Decodificacion de base64
diagnosis_ciphertext = base64.b64decode(data[2])
treatment_ciphertext = base64.b64decode(data[3])
passwordSalt = base64.b64decode(data[4])

# Conversion de datos
iv = int(data[5])

print('Diagnosis encrypted:', binascii.hexlify(diagnosis_ciphertext))
print('Medical treatment encrypted:', binascii.hexlify(treatment_ciphertext))
print('IV',iv)

# El password necesita guardarse en un archivo separado
# y que no se suba a git
password = "s3cr3t*c0d3"

# Se genera la misma la llave de 256 bits con la que se
# cifro el mensaje en claro, para ello se necesita la 
# misma contraseña o password y la misma Salt (passwordSalt)
# para que PBKDF2 derive la misma llave
key = pbkdf2.PBKDF2(password,passwordSalt).read(32)
print('key',binascii.hexlify(key))

# El modo de operación CTR usa el mismo nonce o IV que se
# utilizó para el cifrado.
# Para indicar a Pyaes que se va a usar un IV propio,
# se usa la función pyaes.Counter()
aes = pyaes.AESModeOfOperationCTR(key, pyaes.Counter(iv))


# Descifrado del mensaje en claro
diagnosis = aes.decrypt(diagnosis_ciphertext)
treatment = aes.decrypt(treatment_ciphertext)

print('Diagnosis:', diagnosis)
print('Medical treatment:', treatment)



# Sobrescribir el contenido de las variables para
# evitar que se puedan obtener los datos a través de 
# un volcado de memoria RAM
SecureString.clearmem(password)
SecureString.clearmem(diagnosis)
SecureString.clearmem(treatment)
SecureString.clearmem(key)

print('AES encryption key:', binascii.hexlify(key))
print('Password:', password)
print('Diagnosis:', binascii.hexlify(diagnosis))
print('Treatment:', binascii.hexlify(treatment))


