from Crypto.Cipher import AES
import pyaes, pbkdf2, scrypt, json, binascii, os, secrets, base64, mysql.connector, SecureString

def decrypt_AES_GCM(encryptedMsg, password):
    (kdfSalt, ciphertext, nonce, authTag) = encryptedMsg
    secretKey = scrypt.hash(password, kdfSalt, N=16384, r=8, p=1, buflen=32)
    aesCipher = AES.new(secretKey, AES.MODE_GCM, nonce)
    plaintext = aesCipher.decrypt_and_verify(ciphertext, authTag)
    return plaintext

def limpiar(list):
    for l in list:
        SecureString.clearmem(l)

def get_password_JSON():
    with open('passwords.json') as file:
        data = json.load(file)
        
        password = data['password']

    file.close()

    print('a',password)

    return password

def main():
    elementos = []

    with open('connection_db.json') as file:
        data = json.load(file)

    for db in data['database']:
        user = db['user']
        password_ = db['password']
        host = db['host']
        port = db['port']
        database = db['database']

    try:
        mydb = mysql.connector.connect(
                                    user=user,
                                    password=password_,
                                    host=host,
                                    port=port,
                                    database=database)
        cursor = mydb.cursor()
        cursor.execute("SELECT * from expediente")
        # data = cursor.fetchone()
        
        for r in cursor:
            elementos.append(r)
        
    
    except mysql.connector.Error as err:
        print("Something went wrong: {}".format(err))

    finally:
        if mydb:
            #cursor.close()
            mydb.close()
            print("DBMS connection is closed")

    # Obtener la contraseña desde un json
    password = get_password_JSON()

    print("\ndata\n")
    for elemento in elementos:
        print({
            'Id:': elemento[0],
            'Name:': elemento[1],
            'Diagnosis (base64):': elemento[2],
            'Medical treatment (base64):': elemento[3],
            'PasswordSalt Diagnosis': elemento[4],
            'PasswordSalt Treatment': elemento[5],
            'IV_d': elemento[6],
            'IV_t': elemento[7],
            'authTag_d': elemento[8],
            'authTag_t': elemento[9],
            },'\n')

        diagnosis_ciphertext = base64.b64decode(elemento[2])
        treatment_ciphertext = base64.b64decode(elemento[3])
        passwordSalt_d = binascii.hexlify(base64.b64decode(elemento[4]))
        passwordSalt_t = binascii.hexlify(base64.b64decode(elemento[5]))

        iv_d = base64.b64decode(elemento[6])
        iv_t = base64.b64decode(elemento[7])
        authTag_d = base64.b64decode(elemento[8])
        authTag_t = base64.b64decode(elemento[9])

        print('Diagnosis encrypted:', binascii.hexlify(diagnosis_ciphertext))
        print('Medical treatment encrypted:', binascii.hexlify(treatment_ciphertext))
        print('passwordSalt Diagnosis',passwordSalt_d)
        print('passwordSalt Treatment',passwordSalt_t)
        print('IV_d',iv_d)
        print('IV_t',iv_t)
        print('authTag_d',authTag_d)
        print('authTag_t',authTag_t,'\n')

        print(password+'\n')

        # key = pbkdf2.PBKDF2(password, passwordSalt).read(32)
        # print('key',binascii.hexlify(key))

        diagnosis = decrypt_AES_GCM( ( binascii.unhexlify(passwordSalt_d), diagnosis_ciphertext, binascii.unhexlify(iv_d), binascii.unhexlify(authTag_d)), password)
        treatment = decrypt_AES_GCM( ( binascii.unhexlify(passwordSalt_t), treatment_ciphertext, binascii.unhexlify(iv_t), binascii.unhexlify(authTag_t)), password)

        print('Diagnosis:', diagnosis)
        print('Medical treatment:', treatment)
        print('\n------Fin de Elemento con ID: ', elemento[0],'------\n')

    limpiar([password,diagnosis,treatment,iv_d,iv_t,authTag_d,authTag_t])

    # print('AES encryption key:', binascii.hexlify(key))
    print('Password:', password)
    print('Diagnosis:', binascii.hexlify(diagnosis))
    print('Treatment:', binascii.hexlify(treatment))
    print('Iv Diagnosis:', binascii.hexlify(iv_d))
    print('Iv Treatment:', binascii.hexlify(iv_t))
    print('AuthTag Diagnosis:', binascii.hexlify(authTag_d))
    print('AuthTag Diagnosis:', binascii.hexlify(authTag_t))

main()