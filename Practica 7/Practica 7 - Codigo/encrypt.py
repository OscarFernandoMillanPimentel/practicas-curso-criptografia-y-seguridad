#!/usr/bin/env python3

# Paulo Contreras Flores
# paulo.contreras.flores@gmail.com

import pyaes, pbkdf2, binascii, os, secrets, base64, mysql.connector, SecureString


# El password necesita guardarse en un archivo separado
# y que no se suba a git
password = "s3cr3t*c0d3"

# La idea es que estos datos vengan de otro lado como parte
# de una app mas completa, pero para la practica lo
# podrian dejar asi directo al codigo
name = "Jhon Connor"
diagnosis = "Heridas por ataque de T-800"
treatment = "Paracetamol cada 8 hrs"

# El algoritmo de derivacion de llaves PBKDF2 necesita
# una salt, se genera una salt pseudorandon de 16 bytes, 
# el min requerido son 8 bytes, y el max de 16 bytes
passwordSalt = os.urandom(16)

# Algoritmo de derivacion de llaves PBKDF2, se toman
# 32 bytes o 256 bits. Entonces se va a cifrar AES con
# una llave de 256 bits, o AES-256
key = pbkdf2.PBKDF2(password, passwordSalt).read(32)

# El modo de operacion CTR necesita un nonce (number
# once) o mejor conocido como vector de inicializacion
# o IV (initialization vector), de 256 bits para este
# ejemplo. Se recomienda generarlo "aleatoriamente"
# Para indicar a Pyaes que se va a usar un IV propio,
# se usa la función pyaes.Counter()
iv = secrets.randbits(256)
aes = pyaes.AESModeOfOperationCTR(key, pyaes.Counter(iv))

# Cifrado del mensaje en claro. La separacion por bloques
# lo realiza la propia funcion
diagnosis_ciphertext = aes.encrypt(diagnosis)
treatment_ciphertext = aes.encrypt(treatment)


print('Password:', password)
print('Diagnosis:', diagnosis)
print('Medical treatment:', treatment)
print('PasswordSalt:', binascii.hexlify(passwordSalt))
print('AES encryption key:', binascii.hexlify(key))
print('IV:', iv)
print('Diagnosis encrypted:', binascii.hexlify(diagnosis_ciphertext))
print('Medical treatment encrypted:', binascii.hexlify(treatment_ciphertext))


# Se codifica en base 64 tanto la passwordSalt como el 
# criptograma para guardarlos en la base de datos
passwordSalt = base64.b64encode(passwordSalt)
diagnosis_ciphertext = base64.b64encode(diagnosis_ciphertext)
treatment_ciphertext = base64.b64encode(treatment_ciphertext)


print('PasswordSalt (base64):', passwordSalt)
print('Diagnosis encrypted (base64):', diagnosis_ciphertext)
print('Medical treatment encrypted (base64):', treatment_ciphertext)



# Guardar los datos en una base de datos relacional
try:
    # Estos datos necesitan estar en un archivo separado 
    # del programa y tampoco deben subirse a git
    # El cifrado de la conexion se realizara en otra practica
    mydb = mysql.connector.connect(
                                 user="cripto",
                                 password="mypassword",
                                 host="127.0.0.1",
                                 port="3306",
                                 database="hospital")
    cursor = mydb.cursor()
    insert_query = """ INSERT INTO expediente (nombre, diagnostico, tratamiento, passwordSalt, iv) VALUES (%s,%s,%s, %s, %s)"""
    record_to_insert = (name, diagnosis_ciphertext, treatment_ciphertext, passwordSalt, iv)
    cursor.execute(insert_query, record_to_insert)

    mydb.commit()
    count = cursor.lastrowid

    print("Record inserted successfully with id ", count)


except mysql.connector.Error as err:
  print("Something went wrong: {}".format(err))


finally:
    if mydb:
        cursor.close()
        mydb.close()
        print("DBMS connection is closed")




# Sobrescribir el contenido de las variables para
# evitar que se puedan obtener los datos a través de 
# un volcado de memoria RAM
SecureString.clearmem(password)
SecureString.clearmem(diagnosis)
SecureString.clearmem(treatment)
SecureString.clearmem(key)

print('AES encryption key:', binascii.hexlify(key))
print('Password:', password)
print('Diagnosis:', diagnosis)
print('Treatment:', treatment)

