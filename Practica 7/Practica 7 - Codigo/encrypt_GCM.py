#Author : - Oscar Fernando Millán Pimentel

from Crypto.Cipher import AES
import pyaes, pbkdf2, scrypt, json, binascii, os, secrets, base64, mysql.connector, SecureString

def encrypt_AES_GCM(msg, password):
    kdfSalt = os.urandom(16)
    secretKey = scrypt.hash(password, kdfSalt, N=16384, r=8, p=1, buflen=32)
    aesCipher = AES.new(secretKey, AES.MODE_GCM)
    ciphertext, authTag = aesCipher.encrypt_and_digest(msg)
    return (kdfSalt, ciphertext, aesCipher.nonce, authTag)

def limpiar(list):
    for l in list:
        SecureString.clearmem(l)

def get_password_JSON():
    with open('passwords.json') as file:
        data = json.load(file)
        
        password = data['password']

    file.close()

    print('a',password)

    return password

def main():
    
    password = get_password_JSON()

    # Información a cifrar
    name = "Jhon Connor"
    diagnosis = b'Heridas por ataque de T-800'
    treatment = b'Paracetamol cada 8 hrs'

    # name = "Anakin Skywalker"
    # diagnosis = b'Quemadura por volcan'
    # treatment = b'Colocacion de protesis'

    passwordSalt_d, diagnosis_ciphertext, iv_d, authTag_d = encrypt_AES_GCM(diagnosis, password)
    passwordSalt_t, treatment_ciphertext, iv_t, authTag_t = encrypt_AES_GCM(treatment, password)

    iv_d = binascii.hexlify(iv_d)
    iv_t = binascii.hexlify(iv_t)

    authTag_d = binascii.hexlify(authTag_d)
    authTag_t = binascii.hexlify(authTag_t)

    print('Password:', password)
    print('Diagnosis:', diagnosis)
    print('Medical treatment:', treatment)
    print('\nPasswordSalt Diagnosis:', binascii.hexlify(passwordSalt_d))
    print('\nPasswordSalt Treatment:', binascii.hexlify(passwordSalt_t))
    # print('AES encryption key:', binascii.hexlify(key))
    print('IV_d:', iv_d)
    print('IV_t:', iv_t)
    print('authTag Diagnosis:', authTag_d)
    print('authTag Treatnment:', authTag_t)
    print('\nDiagnosis encrypted:', binascii.hexlify(diagnosis_ciphertext))
    print('Medical treatment encrypted:', binascii.hexlify(treatment_ciphertext))

    passwordSalt_d_ = base64.b64encode(passwordSalt_d)
    passwordSalt_t_ = base64.b64encode(passwordSalt_t)
    diagnosis_ciphertext_ = base64.b64encode(diagnosis_ciphertext)
    treatment_ciphertext_ = base64.b64encode(treatment_ciphertext)
    iv_d_ = base64.b64encode(iv_d)
    iv_t_ = base64.b64encode(iv_t)
    authTag_d_ = base64.b64encode(authTag_d)
    authTag_t_ = base64.b64encode(authTag_t)

    print('\nPasswordSalt Diagnosis (base64):', passwordSalt_d_)
    print('PasswordSalt Treatment (base64):', passwordSalt_t_)
    print('Diagnosis encrypted (base64):', diagnosis_ciphertext_)
    print('Medical treatment encrypted (base64):', treatment_ciphertext_)
    print('Iv_d (base64):', iv_d_)
    print('Iv_t (base64):', iv_t_)
    print('authTag_d (base64):', authTag_d_)
    print('authTag_t (base64):', authTag_t_)

    with open('connection_db.json') as file:
        data = json.load(file)

    for db in data['database']:
        user = db['user']
        password_ = db['password']
        host = db['host']
        port = db['port']
        database = db['database']

    try:
        mydb = mysql.connector.connect(
                                    user=user,
                                    password=password_,
                                    host=host,
                                    port=port,
                                    database=database)
        cursor = mydb.cursor()
        insert_query = """ INSERT INTO expediente (nombre, diagnostico, tratamiento, passwordSalt_d, passwordSalt_t, iv_d, iv_t, authTag_d, authTag_t) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        record_to_insert = (name, diagnosis_ciphertext_, treatment_ciphertext_, passwordSalt_d_, passwordSalt_t_,iv_d_, iv_t_, authTag_d_, authTag_t_)
        cursor.execute(insert_query, record_to_insert)

        mydb.commit()
        count = cursor.lastrowid

        print("Record inserted successfully with id ", count)


    except mysql.connector.Error as err:
        print("Something went wrong: {}".format(err))


    finally:
        if mydb:
            cursor.close()
            mydb.close()
            print("DBMS connection is closed")

    limpiar([password,diagnosis,treatment,passwordSalt_d ,passwordSalt_t ,iv_d,iv_t,authTag_d,authTag_t])

    # print('\nAES encryption key:', binascii.hexlify(key))
    print('\nPassword:', password)
    print('PasswordSalt Diagnosis:', passwordSalt_d)
    print('PasswordSalt Treatment:', passwordSalt_t)
    print('Diagnosis:', diagnosis)
    print('Treatment:', treatment)
    print('Iv Diagnosis:', binascii.hexlify(iv_d))
    print('Iv Treatment:', binascii.hexlify(iv_t))
    print('AuthTag Diagnosis:', binascii.hexlify(authTag_d))
    print('AuthTag Trearment:', binascii.hexlify(authTag_t))

main()