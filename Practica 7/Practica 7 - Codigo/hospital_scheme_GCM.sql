CREATE DATABASE IF NOT EXISTS hospital_gcm;
USE hospital_gcm;
CREATE TABLE IF NOT EXISTS expediente (
  id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(255) NOT NULL,
  diagnostico varchar(450) NOT NULL,
  tratamiento varchar(450) NOT NULL,
  passwordSalt_d varchar(24) NOT NULL,
  passwordSalt_t varchar(24) NOT NULL,
  iv_d varchar(256) NOT NULL,
  iv_t varchar(256) NOT NULL,
  authTag_d varchar(256) NOT NULL,
  authTag_t varchar(256) NOT NULL
);
CREATE USER IF NOT EXISTS 'cripto'@'localhost' IDENTIFIED BY 'mypassword';
GRANT ALL PRIVILEGES ON hospital_gcm.* TO 'cripto'@'localhost';
FLUSH PRIVILEGES;
