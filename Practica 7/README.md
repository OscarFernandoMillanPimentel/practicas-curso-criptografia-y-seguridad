# Cifrado con AES-256 en modo GCM y uso de Scrypt


## Ejecución
```
$ python3 encrypt_GCM.py
$ python3 decrypt_GCM.py
```

## Instalación de paquetes
```
# aptitude install mariadb-server mariadb-client python pip libssl-dev binutils build-essential
```

## Instalación de bibliotecas de python
```
# pip install pyaes
# pip install pbkdf2
# pip install mysql-connector-python
# pip install SecureString
# pip install scrypt
# pip install pycryptodome
```

## Generar esquema de la base de datos
```
# mysql < hospital_scheme_GCM.sql
```

## Versiones
- Mariadb  Ver 15.1 Distrib 10.5.12-MariaDB
- Python 3.9.2
- pip 20.3.4 (python 3.9)
- build-essential 12.9
- binutils 2.35.2-2
- libssl-dev 1.1.1k-1+deb11u1
- pyaes 1.6.1
- pbkdf2 1.3
- mysql-connector-python 8.0.27
- SecureString 0.2


## Referencias
- https://cryptobook.nakov.com/
- https://github.com/ricmoo/pyaes
- https://pypi.org/project/pyaes/
- https://www.sjoerdlangkemper.nl/2016/05/22/should-passwords-be-cleared-from-memory/
- https://www.sjoerdlangkemper.nl/2016/06/09/clearing-memory-in-python/
- https://github.com/dnet/pysecstr
- https://pypi.org/project/SecureString/
